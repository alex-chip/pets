(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.edgrid = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _menu = require('./menu');

var _menu2 = _interopRequireDefault(_menu);

var _migrate = require('./migrate');

var _migrate2 = _interopRequireDefault(_migrate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = { menu: _menu2.default, migrate: _migrate2.default };
module.exports = exports['default'];

},{"./menu":2,"./migrate":3}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _utils = require('./utils');

//TODO: Add null pointer error checked.
exports.default = function (navId, menuId) {
  var nav = (0, _utils.$)('#' + navId),
      menu = (0, _utils.$)('#' + menuId),
      toggleButton = (0, _utils.$)('#' + navId + '-toggle');

  function showNav() {
    nav.classList.toggle('show-menu');
  }

  function showSubMenu(e) {
    if (e.target.classList.contains('expand-submenu')) {
      e.preventDefault();
      e.target.classList.toggle('active');
      e.target.previousElementSibling.classList.toggle('show-submenu');
    }
  }

  // si el nav y toggle existen mostrar u ocultar menu
  if (nav) {
    if (toggleButton) {
      toggleButton.addEventListener('click', showNav);
    } else {
      console.error('Not found ' + navId + '-toggle Id');
    }
  } else {
    console.error('Not found ' + navId + ' Id');
  }

  if (menu) {
    // show submenus
    menu.addEventListener('click', showSubMenu);
    //
    // while (menuItemsLength--) {
    //   let menuItem = menuItems[menuItemsLength];
    //   // Detectar si un item es padre de un submenu
    //   if (menuItem.querySelector('ul') != null) {
    //     menuItem.classList.add('parent-submenu');
    //
    //
    //   }
    // }

    (0, _utils.each)((0, _utils.$)('li', menu) /* menuItems */, function (menuItem) {
      // Detectar si un item es padre de un submenu
      if (menuItem.querySelector('ul') != null) {
        menuItem.classList.add('parent-submenu');

        //Crear toggle button para submenus
        var expandSubmenu = document.createElement('div');
        expandSubmenu.classList.add('expand-submenu');
        menuItem.appendChild(expandSubmenu);
      }
    });
  } else {
    console.error('Not found ' + menuId + ' Id');
  }
};

module.exports = exports['default'];

},{"./utils":4}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _utils = require('./utils');

exports.default = function () {

  // Clases antiguas (como clave) y nuevas (como valor).
  var classes = {
    grupo: 'ed-container',
    caja: 'ed-item',
    total: 'full',
    'hasta-tablet': 'to-m',
    'hasta-web': 'to-l',
    'hasta-hd': 'to-xl',
    'desde-tablet': 'from-m',
    'desde-web': 'from-l',
    'desde-hd': 'from-xl',
    'base-': 's-',
    'movil-': 's-',
    'tablet-': 'm-',
    'web-': 'l-',
    'hd-': 'xl-'
  };

  // Obtenemos las clases antiguas.
  var oldClasses = Object.keys(classes);

  // Expresión regular que encuentra los selectores antiguos
  // a partir del className de un elemento.
  // Creada a partir de: http://stackoverflow.com/questions/195951/change-an-elements-class-with-javascript#answer-196038
  var OLD_SELECTORS_REGEX = new RegExp([

  // Coincide con el inicio del className o
  // con un espacio que precede al selector antiguo.
  '(^|\\s)',

  // Conicide con uno de todos los selectores.
  '(' + oldClasses.join('|') + ')',

  // Coincide con el tamaño asignado por el selector
  // ej: base-50, coincidiría con 50.
  '(\\d{1,3}|\\d-\\d)?',

  // Coincide con el selector antiguo que este seguido
  // por un espacio o con el final del className.
  '(?!\\S)'].join(''), 'g');

  // Creamos un nuevo arreglo con los selectores de atributo
  // y de clase, resultantes de las clases antiguas.
  var oldSelectors = oldClasses.map(function (oldClass) {

    // Verificamos si se necesita un selector de atributo.
    // Si se necesita, lo retornamos con este.
    if (oldClass.endsWith('-')) {
      return '[class*="' + oldClass + '"]';
    }

    // De lo contrario, lo retornamos como selector de clase.
    return '.' + oldClass;
  });

  // Iteramos todos los elementos para añadir las nuevas clases.
  (0, _utils.each)((0, _utils.$)(oldSelectors.join()), function (element) {
    var match = void 0;
    var newClasses = '';

    // Iteramos todas las coincidencias.
    while (match = OLD_SELECTORS_REGEX.exec(element.className)) {
      newClasses += ' ' + (classes[match[2]] + (match[3] || ''));
    }

    // Añadimos las nuevas clases.
    element.className += newClasses;
  });
};

module.exports = exports['default'];

},{"./utils":4}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.$ = $;
exports.each = each;
/**
 * Alias del método `querySelectorAll`
 *
 * @param {String} selector
 * @param {Document|HTMLElement} context
 *
 * @return {NodeList|HTMLElement}
 *
 * @private
 */
function $(selector) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  //if (/^#[\w-]+$/.test(selector)) {
  //return context.getElementById(selector.slice(1))
  //}

  //return context.querySelectorAll(selector)

  return (/^#[\w-]+$/.test(selector) ? context.getElementById(selector.slice(1)) : context.querySelectorAll(selector)
  );
}

/**
 * Recorre todos los elementos de un NodeList o HTMLCollection.
 *
 * @param {NodeList|HTMLCollection} elements
 * @param {Function} callback
 *
 * @return void
 *
 * @private
 */
function each(elements, callback) {
  var length = elements.length;


  for (var i = 0; i < length; ++i) {
    callback(elements[i], i, elements);
  }
}

},{}]},{},[1])(1)
});

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJkaXN0L2pzL2JhYmVsL2luZGV4LmpzIiwiZGlzdC9qcy9iYWJlbC9tZW51LmpzIiwiZGlzdC9qcy9iYWJlbC9taWdyYXRlLmpzIiwiZGlzdC9qcy9iYWJlbC91dGlscy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQ0FBOzs7O0FBQ0E7Ozs7OztrQkFFZSxFQUFFLG9CQUFGLEVBQVEsMEJBQVIsRTs7Ozs7Ozs7OztBQ0hmOztBQUVBO2tCQUNlLFVBQUMsS0FBRCxFQUFRLE1BQVIsRUFBbUI7QUFDaEMsTUFBSSxNQUFNLG9CQUFNLEtBQU4sQ0FBVjtBQUFBLE1BQ0UsT0FBTyxvQkFBTSxNQUFOLENBRFQ7QUFBQSxNQUVFLGVBQWUsb0JBQU0sS0FBTixhQUZqQjs7QUFJQSxXQUFTLE9BQVQsR0FBbUI7QUFDakIsUUFBSSxTQUFKLENBQWMsTUFBZCxDQUFxQixXQUFyQjtBQUNEOztBQUVELFdBQVMsV0FBVCxDQUFxQixDQUFyQixFQUF3QjtBQUN0QixRQUFJLEVBQUUsTUFBRixDQUFTLFNBQVQsQ0FBbUIsUUFBbkIsQ0FBNEIsZ0JBQTVCLENBQUosRUFBbUQ7QUFDakQsUUFBRSxjQUFGO0FBQ0EsUUFBRSxNQUFGLENBQVMsU0FBVCxDQUFtQixNQUFuQixDQUEwQixRQUExQjtBQUNBLFFBQUUsTUFBRixDQUFTLHNCQUFULENBQWdDLFNBQWhDLENBQTBDLE1BQTFDLENBQWlELGNBQWpEO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBLE1BQUksR0FBSixFQUFTO0FBQ1AsUUFBSSxZQUFKLEVBQWtCO0FBQ2hCLG1CQUFhLGdCQUFiLENBQThCLE9BQTlCLEVBQXVDLE9BQXZDO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsY0FBUSxLQUFSLGdCQUEyQixLQUEzQjtBQUNEO0FBQ0YsR0FORCxNQU1PO0FBQ0wsWUFBUSxLQUFSLGdCQUEyQixLQUEzQjtBQUNEOztBQUdELE1BQUksSUFBSixFQUFVO0FBQ1I7QUFDQSxTQUFLLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFdBQS9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUJBQUssY0FBRSxJQUFGLEVBQVEsSUFBUixDQUFMLENBQW1CLGVBQW5CLEVBQW9DLG9CQUFZO0FBQzlDO0FBQ0EsVUFBSSxTQUFTLGFBQVQsQ0FBdUIsSUFBdkIsS0FBZ0MsSUFBcEMsRUFBMEM7QUFDeEMsaUJBQVMsU0FBVCxDQUFtQixHQUFuQixDQUF1QixnQkFBdkI7O0FBRUE7QUFDQSxZQUFJLGdCQUFnQixTQUFTLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBcEI7QUFDQSxzQkFBYyxTQUFkLENBQXdCLEdBQXhCLENBQTRCLGdCQUE1QjtBQUNBLGlCQUFTLFdBQVQsQ0FBcUIsYUFBckI7QUFDRDtBQUNGLEtBVkQ7QUFXRCxHQXpCRCxNQXlCTztBQUNMLFlBQVEsS0FBUixnQkFBMkIsTUFBM0I7QUFDRDtBQUNGLEM7Ozs7Ozs7Ozs7O0FDNUREOztrQkFFZSxZQUFNOztBQUVuQjtBQUNBLE1BQU0sVUFBVTtBQUNkLFdBQU8sY0FETztBQUVkLFVBQU0sU0FGUTtBQUdkLFdBQU8sTUFITztBQUlkLG9CQUFnQixNQUpGO0FBS2QsaUJBQWEsTUFMQztBQU1kLGdCQUFZLE9BTkU7QUFPZCxvQkFBZ0IsUUFQRjtBQVFkLGlCQUFhLFFBUkM7QUFTZCxnQkFBWSxTQVRFO0FBVWQsYUFBUyxJQVZLO0FBV2QsY0FBVSxJQVhJO0FBWWQsZUFBVyxJQVpHO0FBYWQsWUFBUSxJQWJNO0FBY2QsV0FBTztBQWRPLEdBQWhCOztBQWlCQTtBQUNBLE1BQU0sYUFBYSxPQUFPLElBQVAsQ0FBWSxPQUFaLENBQW5COztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQU0sc0JBQXNCLElBQUksTUFBSixDQUFXOztBQUVyQztBQUNBO0FBQ0EsV0FKcUM7O0FBTXJDO0FBTnFDLFFBT2pDLFdBQVcsSUFBWCxDQUFnQixHQUFoQixDQVBpQzs7QUFTckM7QUFDQTtBQUNBLHVCQVhxQzs7QUFhckM7QUFDQTtBQUNBLFdBZnFDLEVBZ0JyQyxJQWhCcUMsQ0FnQmhDLEVBaEJnQyxDQUFYLEVBZ0JoQixHQWhCZ0IsQ0FBNUI7O0FBa0JBO0FBQ0E7QUFDQSxNQUFNLGVBQWUsV0FBVyxHQUFYLENBQWUsb0JBQVk7O0FBRTlDO0FBQ0E7QUFDQSxRQUFJLFNBQVMsUUFBVCxDQUFrQixHQUFsQixDQUFKLEVBQTRCO0FBQzFCLDJCQUFtQixRQUFuQjtBQUNEOztBQUVEO0FBQ0EsaUJBQVcsUUFBWDtBQUNELEdBVm9CLENBQXJCOztBQVlBO0FBQ0EsbUJBQUssY0FBRSxhQUFhLElBQWIsRUFBRixDQUFMLEVBQTZCLG1CQUFXO0FBQ3RDLFFBQUksY0FBSjtBQUNBLFFBQUksYUFBYSxFQUFqQjs7QUFFQTtBQUNBLFdBQU8sUUFBUSxvQkFBb0IsSUFBcEIsQ0FBeUIsUUFBUSxTQUFqQyxDQUFmLEVBQTREO0FBQzFELDJCQUFrQixRQUFRLE1BQU0sQ0FBTixDQUFSLEtBQXFCLE1BQU0sQ0FBTixLQUFZLEVBQWpDLENBQWxCO0FBQ0Q7O0FBRUQ7QUFDQSxZQUFRLFNBQVIsSUFBcUIsVUFBckI7QUFDRCxHQVhEO0FBWUQsQzs7Ozs7Ozs7OztRQy9EZSxDLEdBQUEsQztRQXNCQSxJLEdBQUEsSTtBQWhDaEI7Ozs7Ozs7Ozs7QUFVTyxTQUFTLENBQVQsQ0FBWSxRQUFaLEVBQTBDO0FBQUEsTUFBcEIsT0FBb0IsdUVBQVYsUUFBVTs7QUFDL0M7QUFDRTtBQUNGOztBQUVBOztBQUVGLFNBQU8sYUFBWSxJQUFaLENBQWlCLFFBQWpCLElBQ0MsUUFBUSxjQUFSLENBQXVCLFNBQVMsS0FBVCxDQUFlLENBQWYsQ0FBdkIsQ0FERCxHQUVDLFFBQVEsZ0JBQVIsQ0FBeUIsUUFBekI7QUFGUjtBQUdDOztBQUVEOzs7Ozs7Ozs7O0FBVU8sU0FBUyxJQUFULENBQWUsUUFBZixFQUF5QixRQUF6QixFQUFtQztBQUFBLE1BQ2hDLE1BRGdDLEdBQ3JCLFFBRHFCLENBQ2hDLE1BRGdDOzs7QUFHeEMsT0FBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLE1BQXBCLEVBQTRCLEVBQUUsQ0FBOUIsRUFBaUM7QUFDL0IsYUFBUyxTQUFTLENBQVQsQ0FBVCxFQUFzQixDQUF0QixFQUF5QixRQUF6QjtBQUNEO0FBQ0YiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiaW1wb3J0IG1lbnUgZnJvbSAnLi9tZW51J1xuaW1wb3J0IG1pZ3JhdGUgZnJvbSAnLi9taWdyYXRlJ1xuXG5leHBvcnQgZGVmYXVsdCB7IG1lbnUsIG1pZ3JhdGUgfVxuIiwiaW1wb3J0IHsgJCwgZWFjaCB9IGZyb20gJy4vdXRpbHMnXG5cbi8vVE9ETzogQWRkIG51bGwgcG9pbnRlciBlcnJvciBjaGVja2VkLlxuZXhwb3J0IGRlZmF1bHQgKG5hdklkLCBtZW51SWQpID0+IHtcbiAgbGV0IG5hdiA9ICQoYCMke25hdklkfWApLFxuICAgIG1lbnUgPSAkKGAjJHttZW51SWR9YCksXG4gICAgdG9nZ2xlQnV0dG9uID0gJChgIyR7bmF2SWR9LXRvZ2dsZWApO1xuXG4gIGZ1bmN0aW9uIHNob3dOYXYoKSB7XG4gICAgbmF2LmNsYXNzTGlzdC50b2dnbGUoJ3Nob3ctbWVudScpO1xuICB9XG5cbiAgZnVuY3Rpb24gc2hvd1N1Yk1lbnUoZSkge1xuICAgIGlmIChlLnRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ2V4cGFuZC1zdWJtZW51JykpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGUudGFyZ2V0LmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpO1xuICAgICAgZS50YXJnZXQucHJldmlvdXNFbGVtZW50U2libGluZy5jbGFzc0xpc3QudG9nZ2xlKCdzaG93LXN1Ym1lbnUnKTtcbiAgICB9XG4gIH1cblxuICAvLyBzaSBlbCBuYXYgeSB0b2dnbGUgZXhpc3RlbiBtb3N0cmFyIHUgb2N1bHRhciBtZW51XG4gIGlmIChuYXYpIHtcbiAgICBpZiAodG9nZ2xlQnV0dG9uKSB7XG4gICAgICB0b2dnbGVCdXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBzaG93TmF2KTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc29sZS5lcnJvcihgTm90IGZvdW5kICR7bmF2SWR9LXRvZ2dsZSBJZGApXG4gICAgfVxuICB9IGVsc2Uge1xuICAgIGNvbnNvbGUuZXJyb3IoYE5vdCBmb3VuZCAke25hdklkfSBJZGApXG4gIH1cblxuXG4gIGlmIChtZW51KSB7XG4gICAgLy8gc2hvdyBzdWJtZW51c1xuICAgIG1lbnUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBzaG93U3ViTWVudSk7XG4gICAgLy9cbiAgICAvLyB3aGlsZSAobWVudUl0ZW1zTGVuZ3RoLS0pIHtcbiAgICAvLyAgIGxldCBtZW51SXRlbSA9IG1lbnVJdGVtc1ttZW51SXRlbXNMZW5ndGhdO1xuICAgIC8vICAgLy8gRGV0ZWN0YXIgc2kgdW4gaXRlbSBlcyBwYWRyZSBkZSB1biBzdWJtZW51XG4gICAgLy8gICBpZiAobWVudUl0ZW0ucXVlcnlTZWxlY3RvcigndWwnKSAhPSBudWxsKSB7XG4gICAgLy8gICAgIG1lbnVJdGVtLmNsYXNzTGlzdC5hZGQoJ3BhcmVudC1zdWJtZW51Jyk7XG4gICAgLy9cbiAgICAvL1xuICAgIC8vICAgfVxuICAgIC8vIH1cblxuICAgIGVhY2goJCgnbGknLCBtZW51KSAvKiBtZW51SXRlbXMgKi8sIG1lbnVJdGVtID0+IHtcbiAgICAgIC8vIERldGVjdGFyIHNpIHVuIGl0ZW0gZXMgcGFkcmUgZGUgdW4gc3VibWVudVxuICAgICAgaWYgKG1lbnVJdGVtLnF1ZXJ5U2VsZWN0b3IoJ3VsJykgIT0gbnVsbCkge1xuICAgICAgICBtZW51SXRlbS5jbGFzc0xpc3QuYWRkKCdwYXJlbnQtc3VibWVudScpO1xuXG4gICAgICAgIC8vQ3JlYXIgdG9nZ2xlIGJ1dHRvbiBwYXJhIHN1Ym1lbnVzXG4gICAgICAgIGxldCBleHBhbmRTdWJtZW51ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIGV4cGFuZFN1Ym1lbnUuY2xhc3NMaXN0LmFkZCgnZXhwYW5kLXN1Ym1lbnUnKTtcbiAgICAgICAgbWVudUl0ZW0uYXBwZW5kQ2hpbGQoZXhwYW5kU3VibWVudSk7XG4gICAgICB9XG4gICAgfSlcbiAgfSBlbHNlIHtcbiAgICBjb25zb2xlLmVycm9yKGBOb3QgZm91bmQgJHttZW51SWR9IElkYClcbiAgfVxufVxuIiwiaW1wb3J0IHsgJCwgZWFjaCB9IGZyb20gJy4vdXRpbHMnXG5cbmV4cG9ydCBkZWZhdWx0ICgpID0+IHtcblxuICAvLyBDbGFzZXMgYW50aWd1YXMgKGNvbW8gY2xhdmUpIHkgbnVldmFzIChjb21vIHZhbG9yKS5cbiAgY29uc3QgY2xhc3NlcyA9IHtcbiAgICBncnVwbzogJ2VkLWNvbnRhaW5lcicsXG4gICAgY2FqYTogJ2VkLWl0ZW0nLFxuICAgIHRvdGFsOiAnZnVsbCcsXG4gICAgJ2hhc3RhLXRhYmxldCc6ICd0by1tJyxcbiAgICAnaGFzdGEtd2ViJzogJ3RvLWwnLFxuICAgICdoYXN0YS1oZCc6ICd0by14bCcsXG4gICAgJ2Rlc2RlLXRhYmxldCc6ICdmcm9tLW0nLFxuICAgICdkZXNkZS13ZWInOiAnZnJvbS1sJyxcbiAgICAnZGVzZGUtaGQnOiAnZnJvbS14bCcsXG4gICAgJ2Jhc2UtJzogJ3MtJyxcbiAgICAnbW92aWwtJzogJ3MtJyxcbiAgICAndGFibGV0LSc6ICdtLScsXG4gICAgJ3dlYi0nOiAnbC0nLFxuICAgICdoZC0nOiAneGwtJ1xuICB9O1xuXG4gIC8vIE9idGVuZW1vcyBsYXMgY2xhc2VzIGFudGlndWFzLlxuICBjb25zdCBvbGRDbGFzc2VzID0gT2JqZWN0LmtleXMoY2xhc3Nlcyk7XG5cbiAgLy8gRXhwcmVzacOzbiByZWd1bGFyIHF1ZSBlbmN1ZW50cmEgbG9zIHNlbGVjdG9yZXMgYW50aWd1b3NcbiAgLy8gYSBwYXJ0aXIgZGVsIGNsYXNzTmFtZSBkZSB1biBlbGVtZW50by5cbiAgLy8gQ3JlYWRhIGEgcGFydGlyIGRlOiBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzE5NTk1MS9jaGFuZ2UtYW4tZWxlbWVudHMtY2xhc3Mtd2l0aC1qYXZhc2NyaXB0I2Fuc3dlci0xOTYwMzhcbiAgY29uc3QgT0xEX1NFTEVDVE9SU19SRUdFWCA9IG5ldyBSZWdFeHAoW1xuXG4gICAgLy8gQ29pbmNpZGUgY29uIGVsIGluaWNpbyBkZWwgY2xhc3NOYW1lIG9cbiAgICAvLyBjb24gdW4gZXNwYWNpbyBxdWUgcHJlY2VkZSBhbCBzZWxlY3RvciBhbnRpZ3VvLlxuICAgICcoXnxcXFxccyknLFxuXG4gICAgLy8gQ29uaWNpZGUgY29uIHVubyBkZSB0b2RvcyBsb3Mgc2VsZWN0b3Jlcy5cbiAgICBgKCR7b2xkQ2xhc3Nlcy5qb2luKCd8Jyl9KWAsXG5cbiAgICAvLyBDb2luY2lkZSBjb24gZWwgdGFtYcOxbyBhc2lnbmFkbyBwb3IgZWwgc2VsZWN0b3JcbiAgICAvLyBlajogYmFzZS01MCwgY29pbmNpZGlyw61hIGNvbiA1MC5cbiAgICAnKFxcXFxkezEsM318XFxcXGQtXFxcXGQpPycsXG5cbiAgICAvLyBDb2luY2lkZSBjb24gZWwgc2VsZWN0b3IgYW50aWd1byBxdWUgZXN0ZSBzZWd1aWRvXG4gICAgLy8gcG9yIHVuIGVzcGFjaW8gbyBjb24gZWwgZmluYWwgZGVsIGNsYXNzTmFtZS5cbiAgICAnKD8hXFxcXFMpJ1xuICBdLmpvaW4oJycpLCAnZycpO1xuXG4gIC8vIENyZWFtb3MgdW4gbnVldm8gYXJyZWdsbyBjb24gbG9zIHNlbGVjdG9yZXMgZGUgYXRyaWJ1dG9cbiAgLy8geSBkZSBjbGFzZSwgcmVzdWx0YW50ZXMgZGUgbGFzIGNsYXNlcyBhbnRpZ3Vhcy5cbiAgY29uc3Qgb2xkU2VsZWN0b3JzID0gb2xkQ2xhc3Nlcy5tYXAob2xkQ2xhc3MgPT4ge1xuXG4gICAgLy8gVmVyaWZpY2Ftb3Mgc2kgc2UgbmVjZXNpdGEgdW4gc2VsZWN0b3IgZGUgYXRyaWJ1dG8uXG4gICAgLy8gU2kgc2UgbmVjZXNpdGEsIGxvIHJldG9ybmFtb3MgY29uIGVzdGUuXG4gICAgaWYgKG9sZENsYXNzLmVuZHNXaXRoKCctJykpIHtcbiAgICAgIHJldHVybiBgW2NsYXNzKj1cIiR7b2xkQ2xhc3N9XCJdYFxuICAgIH1cblxuICAgIC8vIERlIGxvIGNvbnRyYXJpbywgbG8gcmV0b3JuYW1vcyBjb21vIHNlbGVjdG9yIGRlIGNsYXNlLlxuICAgIHJldHVybiBgLiR7b2xkQ2xhc3N9YFxuICB9KVxuXG4gIC8vIEl0ZXJhbW9zIHRvZG9zIGxvcyBlbGVtZW50b3MgcGFyYSBhw7FhZGlyIGxhcyBudWV2YXMgY2xhc2VzLlxuICBlYWNoKCQob2xkU2VsZWN0b3JzLmpvaW4oKSksIGVsZW1lbnQgPT4ge1xuICAgIGxldCBtYXRjaFxuICAgIGxldCBuZXdDbGFzc2VzID0gJydcblxuICAgIC8vIEl0ZXJhbW9zIHRvZGFzIGxhcyBjb2luY2lkZW5jaWFzLlxuICAgIHdoaWxlIChtYXRjaCA9IE9MRF9TRUxFQ1RPUlNfUkVHRVguZXhlYyhlbGVtZW50LmNsYXNzTmFtZSkpIHtcbiAgICAgIG5ld0NsYXNzZXMgKz0gYCAke2NsYXNzZXNbbWF0Y2hbMl1dICsgKG1hdGNoWzNdIHx8ICcnKX1gXG4gICAgfVxuXG4gICAgLy8gQcOxYWRpbW9zIGxhcyBudWV2YXMgY2xhc2VzLlxuICAgIGVsZW1lbnQuY2xhc3NOYW1lICs9IG5ld0NsYXNzZXNcbiAgfSlcbn1cbiIsIi8qKlxuICogQWxpYXMgZGVsIG3DqXRvZG8gYHF1ZXJ5U2VsZWN0b3JBbGxgXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IHNlbGVjdG9yXG4gKiBAcGFyYW0ge0RvY3VtZW50fEhUTUxFbGVtZW50fSBjb250ZXh0XG4gKlxuICogQHJldHVybiB7Tm9kZUxpc3R8SFRNTEVsZW1lbnR9XG4gKlxuICogQHByaXZhdGVcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uICQgKHNlbGVjdG9yLCBjb250ZXh0ID0gZG9jdW1lbnQpIHtcbiAgLy9pZiAoL14jW1xcdy1dKyQvLnRlc3Qoc2VsZWN0b3IpKSB7XG4gICAgLy9yZXR1cm4gY29udGV4dC5nZXRFbGVtZW50QnlJZChzZWxlY3Rvci5zbGljZSgxKSlcbiAgLy99XG5cbiAgLy9yZXR1cm4gY29udGV4dC5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKVxuXG5yZXR1cm4gL14jW1xcdy1dKyQvLnRlc3Qoc2VsZWN0b3IpXG4gICAgICA/IGNvbnRleHQuZ2V0RWxlbWVudEJ5SWQoc2VsZWN0b3Iuc2xpY2UoMSkpXG4gICAgICA6IGNvbnRleHQucXVlcnlTZWxlY3RvckFsbChzZWxlY3Rvcik7XG59XG5cbi8qKlxuICogUmVjb3JyZSB0b2RvcyBsb3MgZWxlbWVudG9zIGRlIHVuIE5vZGVMaXN0IG8gSFRNTENvbGxlY3Rpb24uXG4gKlxuICogQHBhcmFtIHtOb2RlTGlzdHxIVE1MQ29sbGVjdGlvbn0gZWxlbWVudHNcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKlxuICogQHJldHVybiB2b2lkXG4gKlxuICogQHByaXZhdGVcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGVhY2ggKGVsZW1lbnRzLCBjYWxsYmFjaykge1xuICBjb25zdCB7IGxlbmd0aCB9ID0gZWxlbWVudHM7XG5cbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBsZW5ndGg7ICsraSkge1xuICAgIGNhbGxiYWNrKGVsZW1lbnRzW2ldLCBpLCBlbGVtZW50cylcbiAgfVxufVxuIl19