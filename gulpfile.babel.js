import gulp from 'gulp'
import pug from 'gulp-pug'
import sass from 'gulp-sass'
import plumber from 'gulp-plumber'
import postcss from 'gulp-postcss'
import autoprefixer from 'autoprefixer'
import cssnano from 'cssnano'
import browserSync from 'browser-sync'
import sourcemaps from 'gulp-sourcemaps'
import browserify from 'browserify'
import babelify from 'babelify'
import buffer from 'vinyl-buffer'
import source from 'vinyl-source-stream'

const server = browserSync.create()

let postcssPlugins = [
  autoprefixer({browsers: 'last 2 versions'}),
  cssnano({core: true})
]

let sassOptions = {
  outputStyle: 'expanded'
}

gulp.task('styles', () =>
  gulp.src('./dev/sass/styles.scss')
      .pipe(plumber({
        errorHandler: function (err) {
          console.log(err)
          this.emit('end')
        }
      }))
      .pipe(sass(sassOptions))
      .pipe(postcss(postcssPlugins))
      .pipe(plumber.stop())
      .pipe(gulp.dest('./dist/css'))
      .pipe(server.stream())
)

gulp.task('pug', () => {
  gulp.src('./dev/pug/index.pug')
  .pipe(pug())
  .pipe(gulp.dest('./dist'))
})

// gulp.task('compileCore', () =>
//   gulp.src('./ed-grid.scss')
//     .pipe(sass(sassOptions))
//     .pipe(postcss(postcssPlugins))
//     .pipe(gulp.dest('./css'))
// )

gulp.task('scripts', () =>
  browserify('./dist/js/babel/index.js', {
    debug: true,
    standalone: 'edgrid'
  })
    .transform(babelify)
    .bundle()
    .pipe(source('ed-grid.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/js'))
)

gulp.task('sw', () =>
  gulp.watch('./dev/sass/**/**.scss', ['styles'])
)

gulp.task('pg', () => {
  gulp.watch('./dev/pug/**/**.pug')
})

gulp.task('default', () => {
  server.init({
    server: {
      baseDir: './dist'
    },

    serveStatic: ['./dist/js']
  })

  gulp.watch('./dev/sass/**/**.scss', ['styles'])
  gulp.watch('./dev/pug/**/**.pug', ['pug'])
  gulp.watch('./dist/js/*.js', ['scripts'])
  gulp.watch('./dist/**.html').on('change', server.reload)
  gulp.watch('./dist/js/*.js').on('change', server.reload)
})
